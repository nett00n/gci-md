#!/bin/sh

CurrentDir=${PWD}
mkdir public

test -f config || cp config.sample config
. config

MDFiles=$(cd content && find . -name "*.md" | awk -F '.md' '{print$1}')
echo ${MDFiles}

test -f content/index.md && echo "There is index.md file in 'content' folder. I cannot work in such environment, sorry" && exit 1

echo "# ${SiteName}" >> content/index.md

for i in ${MDFiles}; do
  echo "* [${i}]($i)" >> content/index.md
  /usr/local/bin/pandoc content/${i}.md -o public/${i}.html
  done

/usr/local/bin/pandoc content/index.md -o public/index.html
